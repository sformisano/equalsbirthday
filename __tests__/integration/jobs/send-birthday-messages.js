import { expect, jest } from '@jest/globals';
import sendBirthdayMessages from '../../../src/jobs/send-birthday-messages';
import MessengerService from '../../../src/services/messenger-service';
import Friend from '../../../src/models/friend';

test('reads data from file system, parses it into friends, and sends birthdays', async () => {
  jest.spyOn(MessengerService, 'sendHappyBirthdayMessage');
  Date.prototype.getMonth = jest.fn(() => 3);
  Date.prototype.getDate = jest.fn(() => 18);

  await sendBirthdayMessages('./data/test-friends.txt');
  expect(MessengerService.sendHappyBirthdayMessage)
    .toHaveBeenNthCalledWith(1, new Friend('Draghi', 'Mario', 'Mario, 1948/04/18', 'draghi@foo.test'));
});
