import { expect } from '@jest/globals';
import Friend from '../../../src/models/friend';
import Birthday from '../../../src/models/birthday.js';

test('Is constructed properly and exposes the expected properties', () => {
  const bestFriend = new Friend(
    'Formisano',
    'Vittorio',
    '2021/09/23',
    'vittorio@foo.test'
  );

  expect(bestFriend.firstName).toEqual('Vittorio');
  expect(bestFriend.lastName).toEqual('Formisano');
  expect(bestFriend.birthday).toBeInstanceOf(Birthday);
  expect(bestFriend.email).toEqual('vittorio@foo.test');
});
