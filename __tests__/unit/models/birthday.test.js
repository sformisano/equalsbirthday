import { expect, jest } from '@jest/globals';
import Birthday from '../../../src/models/birthday.js';

test('can be constructed from month index and month day', () => {
  const birthday = new Birthday(12, 18);

  expect(birthday.monthIndex).toBe(12);
  expect(birthday.monthDay).toBe(18);
});

test('can be constructed from birthday string', () => {
  const birthday = Birthday.fromTextBirthday('1975/09/11');
  expect(birthday.monthIndex).toBe(8);
  expect(birthday.monthDay).toBe(11);
});

test('birthday is today if birthday date matches today', () => {
  Date.prototype.getMonth = jest.fn(() => 6);
  Date.prototype.getDate = jest.fn(() => 4);

  const birthday = new Birthday(6, 4);

  expect(birthday.isToday()).toBe(true);
});

test('birthday is today if birthday date is Feb 29 and today is Feb 28', () => {
  Date.prototype.getMonth = jest.fn(() => 1);
  Date.prototype.getDate = jest.fn(() => 28);

  const birthday = new Birthday(1, 29);

  expect(birthday.isToday()).toBe(true);
});
