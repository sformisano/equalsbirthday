import { expect, jest } from '@jest/globals';
import FriendRepository from '../../../src/repositories/friend-repository.js';
import Friend from '../../../src/models/friend';

const friends = `
Gervasio, Ricky, 1982/11/14, gervais@foobar.com
Cappello, Dave, 1982/07/19, chappelle@foobar.com
Pietra, Chris, 1982/05/24, rock@foobar.com
`;

const friendsWithWhitespace = `
    Gervasio,  Ricky,    1982/11/14,   gervais@foobar.com
    Cappello,      Dave, 1982/07/19, chappelle@foobar.com
    Pietra,      Chris,        1982/05/24,      rock@foobar.com
`;

const friendsWithLineBreaks = `

    Gervasio,  Ricky,    1982/11/14,   gervais@foobar.com
    
    Cappello,      Dave, 1982/07/19, chappelle@foobar.com
    
    
    
    
    Pietra,      Chris,        1982/05/24,      rock@foobar.com
    
    
`;

function expectedFriends(result) {
  expect(result.length).toBe(3);

  expect(result[0].firstName).toBe('Ricky');
  expect(result[0].lastName).toBe('Gervasio');
  expect(result[0].birthday.monthIndex).toEqual(10);
  expect(result[0].birthday.monthDay).toEqual(14);

  expect(result[1].firstName).toBe('Dave');
  expect(result[1].lastName).toBe('Cappello');
  expect(result[1].birthday.monthIndex).toEqual(6);
  expect(result[1].birthday.monthDay).toEqual(19);

  expect(result[2].firstName).toBe('Chris');
  expect(result[2].lastName).toBe('Pietra');
  expect(result[2].birthday.monthIndex).toEqual(4);
  expect(result[2].birthday.monthDay).toEqual(24);
}

describe('getAll', () => {
  test('parses friends data', () => {
    FriendRepository.prototype.getRawData = jest.fn().mockReturnValue(friends);

    const result = new FriendRepository('').getAll();
    expectedFriends(result);
  });


  test('ignores whitespace', () => {
    FriendRepository.prototype.getRawData = jest.fn().mockReturnValue(friendsWithWhitespace);

    const result = new FriendRepository('').getAll();
    expectedFriends(result);
  });

  test('ignores line breaks', () => {
    FriendRepository.prototype.getRawData = jest.fn().mockReturnValue(friendsWithLineBreaks);
    const result = new FriendRepository('').getAll();
    expectedFriends(result);
  });
});

describe('getByBirthday', () => {
  test('returns only friends whose birthday is today', () => {
    Date.prototype.getMonth = jest.fn(() => 9);
    Date.prototype.getDate = jest.fn(() => 30);

    FriendRepository.prototype.getAll = jest.fn().mockReturnValue([
      new Friend('Del Piero', 'Alessandro', '1974/11/09', 'delpiero@foo.test'),
      new Friend('Lavezzi', 'Ezequiel', '1985/05/03', 'lavezzi@foo.test'),
      new Friend('Maradona', 'Diego', '1960/10/30', 'diego@foo.test'),
      new Friend('D10S', 'Mano', '1960/10/30', 'd10s@foo.test'),
      new Friend('Cavani', 'Edison', '1987/02/14', 'cavani@foo.test'),
    ]);

    const friends = new FriendRepository('').getByBirthday();
    expect(friends.length).toBe(2);
    expect(friends[0].lastName).toBe('Maradona');
    expect(friends[1].lastName).toBe('D10S');
  });
});
