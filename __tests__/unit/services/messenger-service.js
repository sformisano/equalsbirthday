import MessengerService from '../../../src/services/messenger-service';
import { jest, expect } from '@jest/globals';
import Friend from '../../../src/models/friend.js';

test('sendEmail logs detailed info on email being sent and resolves with delivery confirmation', async () => {
  jest.spyOn(console, 'log');
  const to = 'foo@mail.com';
  const subject = 'Hello';
  const content = 'Hello Foo';

  const result = await MessengerService.sendEmail(to, subject, content);

  expect(result).toBe(`email to "${to}" sent`);

  expect(console.log).toHaveBeenNthCalledWith(
    1,
    `| sending email to "${to}", subject "${subject}", content "${content}"...`
  );
});

test('sendHappyBirthdayMessage calls sendEmail, logs and returns its result', async () => {
  jest.spyOn(console, 'log');
  MessengerService.sendEmail = jest.fn().mockResolvedValue('definitely sent');

  const friend = new Friend(
    'Doe',
    'John',
    '1980/11/11',
    'doe@foo.test'
  );
  const result = await MessengerService.sendHappyBirthdayMessage(friend);

  expect(MessengerService.sendEmail).toHaveBeenCalledWith(
    friend.email,
    'Happy birthday!',
    'Happy birthday, dear John!'
  );

  expect(result).toBe('definitely sent');

  expect(console.log).toHaveBeenNthCalledWith(
    1,
    `> ${result}`
  );
});
