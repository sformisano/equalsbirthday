import {jest} from '@jest/globals';
import FriendRepository from '../../../src/repositories/friend-repository.js';
import sendBirthdayMessages from '../../../src/jobs/send-birthday-messages';
import Friend from '../../../src/models/friend.js';
import MessengerService from '../../../src/services/messenger-service';

const friends = [
  new Friend('Diamond', 'Neil', '1982/02/29', 'neil@foo.test'),
  new Friend('Cook', 'Tim', '1982/02/29', 'tim@foo.test'),
  new Friend('Wozniak', 'Steve', '1982/02/29', 'woz@foo.test'),
];

beforeEach(() => {
  jest.spyOn(console, 'log');
  jest.spyOn(console, 'error');
  FriendRepository.prototype.getByBirthday = jest.fn().mockReturnValue(friends);
  MessengerService.sendHappyBirthdayMessage = jest.fn();
});

test('it prints initial job execution log', () => {
  sendBirthdayMessages().then(() => {
    expect(console.log).toHaveBeenNthCalledWith(
      1,
      '--- EXECUTING "SEND BIRTHDAY MESSAGES" JOB ---'
    );
  });
});

test('logs message and returns false without friends with birthdays', () => {
  FriendRepository.prototype.getByBirthday.mockReturnValue([]);
  sendBirthdayMessages().then(res => {
    expect(console.log).toHaveBeenNthCalledWith(
      2,
      '> no friends with birthday today: job done'
    );
    expect(res).toBe(false);
  });
});

test('delivers a message for each friend with birthday and logs success message', () => {
  sendBirthdayMessages().then(() => {
    expect(MessengerService.sendHappyBirthdayMessage)
      .toHaveBeenCalledTimes(3);

    expect(MessengerService.sendHappyBirthdayMessage)
      .toHaveBeenNthCalledWith(1, friends[0]);

    expect(MessengerService.sendHappyBirthdayMessage)
      .toHaveBeenNthCalledWith(2, friends[1]);

    expect(MessengerService.sendHappyBirthdayMessage)
      .toHaveBeenNthCalledWith(3, friends[2]);

    expect(console.log).toHaveBeenNthCalledWith(
      4,
      '> job succeeded: 3 messages were delivered'
    );
  });
});

test('logs an error message if any of the message deliveries fails', () => {
  FriendRepository.prototype.getByBirthday.mockReturnValue(friends);
  MessengerService.sendHappyBirthdayMessage.mockRejectedValue('woz plays the instrument, steve plays the orchestra');
  sendBirthdayMessages().then(() => {
    expect(console.error).toHaveBeenNthCalledWith(
      1,
      '> job failed: woz plays the instrument, steve plays the orchestra'
    );
  });
});
