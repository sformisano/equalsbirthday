/**
 * The birthday model felt custom enough, given the Feb 29th constraint and any
 * likely future improvements to warrant its own implementation to make core
 * more readable.
 */
export default class Birthday {
  /**
   *
   * @param birthdayText
   * @returns {Birthday}
   */
  static fromTextBirthday(birthdayText) {
    const birthdayDateItems = birthdayText.split('/');
    const birthdayMonthIndex = parseInt(birthdayDateItems[1], 10) - 1;
    const birthdayMonthDay = parseInt(birthdayDateItems[2], 10);

    return new Birthday(birthdayMonthIndex, birthdayMonthDay);
  }

  constructor(monthIndex, monthDay) {
    this.monthIndex = monthIndex ;
    this.monthDay = monthDay;
  }

  /**
   * Tells the called whether the birthday date matches today's date.
   * Note: for friends born on February 29th, we want to send birthday wishes
   * on February 28th as per spec, hence the below logic to turn Feb 29th into
   * Feb 28th for the birthday check.
   *
   * @returns {boolean}
   */
  isToday() {

    const monthIndex = this.monthIndex;
    let monthDay = this.monthIndex === 1 && this.monthDay === 29
      ? 28
      : this.monthDay;

    const today = new Date();
    return monthIndex === today.getMonth() && monthDay === today.getDate();
  }
}
