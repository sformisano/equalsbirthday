import Birthday from './birthday.js';

/**
 * A simple class encapsulating friend's data
 */
export default class Friend {
  constructor(lastName, firstName, birthdayText, email) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.birthday = Birthday.fromTextBirthday(birthdayText);
    this.email = email;

  }
}
