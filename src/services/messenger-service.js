let timeout = 1000;

/**
 * Delivers messages to friends
 */
export default class MessengerService {

  /**
   * Method simulating email delivery with a growing delay for each new call
   * his could hook into postmark, AWS simple email service, etc.
   *
   * @param {string} to
   * @param {string} subject
   * @param {string} content
   * @returns {Promise<string>}
   */
  static async sendEmail(to, subject, content) {
    // simulate delays for each new request
    timeout += 2000;

    // call to postmark/sendgrid/aws ses would go here
    return new Promise((res) => {
      console.log(`| sending email to "${to}", subject "${subject}", content "${content}"...`);
      setTimeout(() => {
        res(`email to "${to}" sent`);
      }, timeout);
    });
  }

  /**
   * Sends out a happy birthday message
   *
   * @param {Friend} friend
   */
  static async sendHappyBirthdayMessage(friend) {
    const result = await MessengerService.sendEmail(
      friend.email,
      'Happy birthday!',
      `Happy birthday, dear ${friend.firstName}!`
    );

    console.log(`> ${result}`);

    return result;
  }
}
