import FriendRepository from '../repositories/friend-repository.js';
import MessengerService from '../services/messenger-service.js';

/**
 * Job that gets all friends whose birthday is today and sends out a happy birthday message
 */
export default async function sendBirthdayMessages(dataPath) {
  console.log('--- EXECUTING "SEND BIRTHDAY MESSAGES" JOB ---');

  try {
    const repository = new FriendRepository(dataPath);
    const friends = repository.getByBirthday();
    if (!friends.length) {
      console.log('> no friends with birthday today: job done');
      return false;
    }

    console.log(`| you have ${friends.length} friends with birthdays today!`);
    console.log('| sending birthday messages...');

    // In the real world, each of these deliveries would be handled as a
    // separate, retryable delayed job pushed to a queue. They would not be all
    // handled in parallel. Also, an extra reporting job would also likely be
    // enqueued to keep track of the deliveries, follow up with the system
    // owner once all deliveries are successful or once the retries are
    // exhausted and the owner must receive a report on the number of successes
    // and number of failures.
    //
    // As it stand, a single failure in these requests is going to trigger
    // the catch block. This is good enough as a proof of concept to show
    // the failure path, but nothing nearly ready for production
    await Promise.all(friends.map(
      friend => MessengerService.sendHappyBirthdayMessage(friend)
    ));

    console.log(`> job succeeded: ${friends.length} messages were delivered`);
  } catch (e) {
    console.error(`> job failed: ${e}`);
  }
}
