import cron from 'node-cron';
import sendBirthdayMessages from './jobs/send-birthday-messages.js';

// Note: the current cron expression triggers the job very frequently to allow
// easier testing. The expression below is the one I would use in production
// const EVERY_DAY_AT_10 = '00 00 10 * * *';
const EVERY_DAY_AT_10 = '10 * * * * *';
cron.schedule(
  EVERY_DAY_AT_10,
  async function() {
    await sendBirthdayMessages('./data/friends.txt');
  },
  null
);

console.log('==================================================');
console.log('EqualsMoney Birthday Service Started');
console.log('==================================================');
