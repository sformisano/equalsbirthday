import fs from 'fs';
import Friend from '../models/friend.js';

/**
 * Provides access to friends data
 */
export default class FriendRepository {
  constructor(friendsDataPath) {
    this.friendsDataPath = friendsDataPath;
  }

  /**
   * Returns all available friends.
   *
   * In the real world this would be a query to the database and it would return
   * paginated results.
   *
   * @returns {string}
   */
  getRawData() {
    try {
      return fs.readFileSync(this.friendsDataPath, { encoding:'utf8', flag:'r' });
    } catch (e) {
      console.error(`Failed to instantiate friend repository: ${e}`);
      throw e;
    }
  }


  /**
   * Returns all available friends.
   *
   * In the real world this would be a query to the database and it would return
   * paginated results.
   *
   * @returns {Friend[]}
   */
  getAll() {
    return this.getRawData()
      .split('\n')
      .filter(friendRecord => friendRecord.trim() !== '')
      .map(friendRecord => friendRecord.split(',').map(friendProperty => friendProperty.trim()))
      .map(friendProperties => {
        return new Friend(
          friendProperties[0],
          friendProperties[1],
          friendProperties[2],
          friendProperties[3]
        );
      });
  }

  /**
   * Returns friends whose birthday is today.
   *
   * In the real world this would be a database query, we would not dump all
   * friends in memory to then filter them in code.
   *
   * @returns {Friend[]}
   */
  getByBirthday() {
    return this
      .getAll()
      .filter(friend => friend.birthday.isToday());
  }
}
