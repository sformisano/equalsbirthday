# Birthday Message Delivery Service

This delivery service does the following:

* It runs a cron job every day to parse a list of people data (name, birthday date, email) from a text file
* If it's anyone's birthday today, it sends them a "happy birthday" message via email
* It handles the February 29th exception by sending the greeting on February 28th

This is a minimal, proof of concept implementation with many flaws and shortcomings. Tests are basic and could be
better structured/organised, there should be better decoupling between the services and their providers... this is an MVP.

Following below is a list of details that paint a high level picture of what a real production-ready service would look like


### Instructions

* clone this repository locally with `git clone https://gitlab.com/sformisano/equalsbirthday.git`
* cd into the root directory of the project
* run `npm install`
* to run the project, execute `npm run start`
* to run the project in dev mode, execute `npm run dev`
* to run the tests, execute `npm run test`


### Basic assumptions

* The outer system is accountable for keeping this service healthy and operational, monitoring it, restarting it if needed etc.
* The outer system is configured in the proper timezone (i.e. the timezone where the friends live)

### Improvements for a better, more scalable and more reliable system

**From a product perspective**

* Augment people data to include their timezone, so that the delivery time can be adjusted
* Send a report to system owner to provide info on birthdays if deliveries happen

**From a technical perspective**

* Observability: the service should provide structured logging, metrics on things like execution time and failures, etc
* Validations: the service should make sure friends data is valid and usable and returning errors otherwise
* Batching: the service should switch to a database, reading it asynchronously, allowing to batch the read process
* Enqueuing: message delivery jobs should be pushed to a queue, and node workers should consume the queue to deliver messages.
* Idempotency: there should be checks in place to make sure, as the service moves towards a message architecture, that
the at least once semantic we would adopt in this scenario does not result in the same email being sent more than once
* Retry-ability: jobs should not be marked as finished until the external message delivery api confirms the delivery is successful.
If the delivery request goes in timeout, an exponential backoff retry system should be in place to retry the delivery multiple times.
* Timezone support: if/when the people data includes the timezone, the daily job logic should switch to checking for
tomorrow's birthdays, and the system should migrate to a multiple timezones queue approach (one queue per timezone could be a minimally viable solution, another solution could be running a single job every hour to account for timezones)
to schedule the one-off jobs for each individual birthday message based on the person's timezone.
* Testing: if this was mission critical, I would have a few more unit tests, way more integration test, and at least a basic e2e test making sure the delivery via email works properly.
